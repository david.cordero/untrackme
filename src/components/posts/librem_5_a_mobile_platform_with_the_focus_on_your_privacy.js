'use strict';

export default {
    title: 'Librem 5, a mobile platform which focuses on your privacy',
    body: `
        <p>The world of operating systems for mobile devices has been dominated by Apple and Google for the last ten years. </p>
        <p>Both android and iOS have been around for quite a while already, and in their current versions, 9 and 12 respectively, they have reached a huge level of stability and customer satisfaction.</p>
        <p>They also offer a huge number of Apps available in their App stores which make of them great platforms to smoothly achieve every kind of daily tasks.</p>

        <h3>A world of tracking systems</h3>
        <p>Sadly, apart from these advantages, one of the characteristics of these two dominant mobile platforms of our times, is their strong tendency to track their users, both actively and passively.</p>
        <p>Just four months ago, <a href="https://digitalcontentnext.org/">digitalcontentnext.org</a> published <a href="https://digitalcontentnext.org/blog/2018/08/21/google-data-collection-research/">a great research</a> which focuses on how much data Google is collecting about their consumers in the life of a typical user. I strongly encourage you to read it to have a better understanding of the situation.</p>
        <p>According to the results of this research, Google, making use of android among other tools, is tracking intensively their users. Android is said to be sending back home, in conjunction with other data, up to 340 times per day the physical location of a typical user’s device.</p>
        <p>On the other side iOS, which might be tracking their users 10 times fewer than android according to this research, was also discovered tracking information of their users. According to the study iOS is sending, among other data, the device location once per day back to Apple servers.</p>
        <p>With all this information, we can say these are definitely hard times for people’s privacy.</p>

        <h3>Beyond android and iOS</h3>
        <p>Even though nowadays android and iOS get together a quite high quota of the mobile os market share, they are not completely alone, and there are in fact some other players in the game.</p>
        <p>There are for example <a href="https://en.wikipedia.org/wiki/Firefox_OS">Firefox OS</a> and <a href="https://en.wikipedia.org/wiki/Ubuntu_Touch">Ubuntu Touch</a>, who sadly did not get too much success, and after a while they were discontinued. They are both nowadays being maintained by the community.</p>
        <p>But those are not the only two attempts to offer an alternative to android and iOS, there are many others, many of them still active.</p>
        <p>With different goals, they want to offer a different approach to the two dominant platforms. That is the case of <a href="https://en.wikipedia.org/wiki/Tizen">Tizen</a>, <a href="https://en.wikipedia.org/wiki/Sailfish_OS">Sailfish</a>, <a href="https://en.wikipedia.org/wiki/WebOS">webOS</a>, etc…</p>
        <p>In this context, we can speak about <a href="https://puri.sm/">Purism</a>, which are currently developing and will be soon releasing their new device, <a href="https://puri.sm/products/librem-5/">Librem 5</a></p>

        <h3>Purism and Librem 5</h3>
        <p>Purism is a company which has been around for a few years already, building products with the goal of protecting their users privacy.</p>
        <p>In this context, they released the laptops Librem 13 and Librem 15 and the new Librem key.</p>
        <p><img src="/images/librem-5-a-mobile-platform-with-the-focus-on-your-privacy-001.png" alt="Librem 5, second preview image"></p>
        <p>With Librem 5, they want to extend now their product line up to the mobile platforms with the same goal. That means building a mobile device which respects your privacy and does not track you.</p>
        <p>Librem 5, still in development, is planned to be released on April 2019</p>

        <h4>What makes Librem 5 different?</h4>
        <p>One might be thinking… "but is it not the same thing that failed with Ubuntu phone and Firefox OS already?"</p>
        <p>Well, I would say it is definitely not, especially in terms of the goal of the project.</p>
        <p>The main difference between the project of Purism and the other alternatives to iOS and android, is that for Purism the goal is respecting the privacy of their users and not the usage of a specific toolset.</p>
        <p>Releasing a phone with GNU/Linux has been the goal of previous projects. GNU/Linux is indeed a very good tool, probably the best, to get a platform that respect your privacy. But do not forget, that Linux is also the base of every Android device, which make of it also one of the best tools, probably the best, to track the users.</p>
        <p>GNU/Linux can be used in summary for good and evil, so its use itself can not be a goal but the way to get a more ambitious objective.</p>
        <p>I think this is the main difference between Purism and the others alternatives to iOS and android. Because Purism, with their GNU/Linux distribution PureOS as a tool, focuses on the goal of protecting users’ privacy.</p>
        <p>PureOS, distribution based on Debian and endorsed by the FSF, is the OS competing against iOS and Android for privacy, and it will be the default on the Librem 5 and the same OS for laptops.</p>
        <p>Purism is a Social Purpose Company, which means it has a legal structure to benefit society rather than exploit it like the big tech C corps.</p>
        <h3>Conclusion</h3>
        <p>Librem 5 is a very promising project, that hopefully will offer an exit out of the tracking of the two current dominant mobile platforms, iOS and android.</p>
        <p>In contrast to previous attempts, the goal of Purism goes beyond the toolset, and focuses on a more ambitious goal which is users‘ privacy. That is something we should always keep in mind because often happens that one cannot see the wood (the goal) for the trees (the tools)…</p>
        <p>I heard already some people complaining about tooling details like using a default Gnome or KDE based desktop environments for PureOS on the Librem 5… please let’s not allow those details to hide the main goal of the project, and let’s keep the focus on what is really important.</p>
    `
};
