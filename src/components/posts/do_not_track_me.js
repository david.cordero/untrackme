'use strict';

export default {
    title: 'Do not track me',
    body: `
        <p>
        Lorem ipsum dolor sit amet consectetur adipiscing, elit cursus a metus convallis varius, suscipit est 
        platea eget fermentum. Montes nisi ultrices libero dapibus fusce varius, et facilisi proin ultricies 
        pellentesque, cubilia nibh blandit scelerisque aliquam. Nascetur mus varius natoque lacus gravida 
        ornare nunc, turpis aliquet in justo tincidunt risus elementum nisi, eros nisl suscipit nullam integer 
        tellus.
        </p>
        <p>Luctus curae molestie phasellus quis magnis habitasse porta habitant faucibus dignissim sodales 
        pretium, lectus mi sapien consequat parturient suspendisse senectus conubia netus iaculis rutrum. 
        Lacus facilisis fermentum aliquet varius est etiam nulla, augue congue sed fringilla pretium eleifend 
        eu, penatibus id iaculis mi netus tempus. Platea nibh fames nullam aenean a facilisi auctor integer 
        primis eget elementum blandit, phasellus malesuada sapien velit sed eros dis volutpat bibendum turpis 
        senectus. Egestas inceptos lacus potenti placerat varius ridiculus facilisi sem sodales orci vehicula, 
        blandit natoque nostra massa elementum hendrerit parturient leo aliquam vivamus, fringilla enim mus 
        pharetra congue dui cubilia donec velit non.</p>
        <p>Dignissim senectus est diam penatibus egestas 
        magna, hac nec felis luctus orci, vestibulum condimentum ullamcorper mi ad. Ultricies class cursus 
        vitae blandit praesent facilisis eget malesuada quisque nec, gravida senectus augue orci in eu aenean 
        donec fames tempor, tincidunt est arcu suspendisse nascetur nunc lacinia sodales pharetra. Faucibus 
        fringilla nascetur parturient dictum placerat netus duis, at quam rutrum quis sapien ornare suspendisse 
        tortor, elementum metus suscipit vel litora vestibulum. Pulvinar nibh euismod ullamcorper laoreet 
        purus lobortis tempor vitae tempus lacinia dis suscipit accumsan vel, dapibus ante aliquam rhoncus 
        malesuada auctor mi imperdiet diam non ut porttitor nec.
        </p>
        <p>Vehicula bibendum et convallis potenti 
        quam, non ligula facilisis tempor netus semper, aliquet lobortis conubia justo. Sociosqu at mus ridiculus 
        magna torquent dignissim vivamus vulputate, ornare ut cubilia interdum ac vehicula litora luctus dui, 
        fermentum ultricies primis bibendum sodales facilisi dapibus. Fermentum pretium vivamus posuere nibh 
        nec tellus ac imperdiet, sagittis fusce curae scelerisque conubia enim tincidunt, per et etiam nascetur 
        habitant vulputate condimentum. Fames ornare laoreet netus viverra imperdiet pellentesque aenean tellus, 
        iaculis etiam potenti platea sollicitudin montes pretium, convallis quisque commodo fringilla elementum 
        nascetur scelerisque.
        </p>
    `
};
