'use strict';

import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/components/Home.vue';
import OnlineServices from '@/components/OnlineServices.vue';
import Applications from '@/components/Applications.vue';
import Mobile from '@/components/Mobile.vue';
import OperatingSystems from '@/components/OperatingSystems.vue';
import Hardware from '@/components/Hardware.vue';
import Post from '@/components/Post.vue';
import NotFound from '@/components/NotFound.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    scrollBehavior() {
        return { x: 0, y: 0 };
    },
    mode: 'history',
    routes: [
        { path: '/', component: Home },
        { path: '/onlineservices', component: OnlineServices },
        { path: '/applications', component: Applications },
        { path: '/mobile', component: Mobile },
        { path: '/operatingsystems', component: OperatingSystems },
        { path: '/hardware', component: Hardware },
        { path: '/posts/:identifier', component: Post },
        { path: '*', component: NotFound }
    ]
});

export default router;
